<?php
   include('session.php');

?>
<html>
   
   <head>
           <title>Bienvenido</title>
           <meta charset="UTF-8">
           <meta name="viewport" content="width=device-width, initial-scale=1">
           <!--===============================================================================================-->
           <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
           <!--===============================================================================================-->
           <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
           <!--===============================================================================================-->
           <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
           <!--===============================================================================================-->
           <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
           <!--===============================================================================================-->
           <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
           <!--===============================================================================================-->
           <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
           <!--===============================================================================================-->
           <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
           <!--===============================================================================================-->
           <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
           <!--===============================================================================================-->
           <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
           <!--===============================================================================================-->
           <link rel="stylesheet" type="text/css" href="css/util.css">
           <link rel="stylesheet" type="text/css" href="css/main.css">
           <!--===============================================================================================-->

   </head>
   
   <body>
   <div>
       <h1>&nbsp;&nbsp;&nbsp;Bienvenido <?php echo $perfil." - ".$login ;?></h1>
       <h2><a href = "logout.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desconectar</a>
       <a href = "Manual_administrador.pdf" target="_blank">Ayuda</a></h2>
       
   </div>


	  <?php if($_SESSION['login_perfil'] == 1)
	  {
		  ?>


          <ul>
              <p style="color: yellow">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Menu principal</p>
              <li><h3><a href = "crear.php">Crear usuario</a></h3></li>
              <li><h3><a href = "CrearCliente.php">Crear Cliente</a></h3></li>
              <li><h3><a href = "listaUsuario.php">Consultar Usuario</a></h3></li>
              <li><h3><a href = "import.php">Importar</a></h3></li>
              <li><h3><a href = "export.php">Exportar</a></h3></li>
              <li><h3><a href = "bitacora.php">Bitacora</a></h3></li>
          </ul>
      <div class="container-login100-form-btn">
          <br/><br/><br/><br/><br/>
      </div>
          <ul>
              <p style="color: yellow">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Menu Secundario</p>
              <li><h3><a href = "Venta.php">Realizar venta</a></h3></li>
              <li><h3><a href = "Listado-ventas.php">Listado de Ventas</a></h3></li>
              <li><h3><a href = "Cotizaciones.php">Cotizaciones</a></h3></li>
              <li><h3><a href = "Bodega.php">Consultar Bodega</a></h3></li>
              <li><h3><a href = "Productos.php">Productos</a></h3></li>
              <li><h3><a href = "Stock.php">Stock Productos</a></h3></li>
              <li><h3><a href = "Estado-caja.php">Consultar estado de caja</a></h3></li>
              <li><h3><a href = "Movimientos-caja.php">Flujo de caja</a></h3></li>
              <li><h3><a href = "Formas-pago.php">Formas de Pago</a></h3></li>
              
              
          </ul>
	  
	  <?php
	  }
	  if($_SESSION['login_perfil'] == 2)
	  {
		  ?>
          
      <ul> 
          <p style="color: yellow">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Menu principal</p>
          <li><h3><a href = "Venta.php">Realizar venta</a></h3></li>
          <li><h3><a href = "Cotizaciones.php">Cotizaciones</a></h3></li>
          <li><h3><a href = "CrearCliente.php">Crear Cliente</a></h3></li>
      </ul>
      <div class="container-login100-form-btn">
          <br/><br/><br/><br/><br/>
      </div>
          
          <ul>
              <p style="color: yellow">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Menu Secundario</p>
              <li><h3><a href = "Bodega.php">Consultar Bodega</a></h3></li>
              <li><h3><a href = "Listado-ventas.php">Listado de Ventas</a></h3></li>
              
          </ul>
	  <?php
	  }
	  
	  if($_SESSION['login_perfil'] == 3)
	  {
		  ?>
          
          <ul>
              <p style="color: yellow">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Menu principal</p>
              <li><h3><a href = "Productos.php">Productos</a></h3></li>
          </ul>
          <div class="container-login100-form-btn">
          <br/><br/><br/><br/><br/>
      </div>
          
          <ul>
          <p style="color: yellow">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Menu Secundario</p>
              <li><h3><a href = "Stock.php">Stock Productos</a></h3></li>
           
          </ul>
	  <?php
	  }
	  
	  if($_SESSION['login_perfil'] == 4)
	  {
		  ?>
          
          <ul>
              <p style="color: yellow">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Menu principal</p>
              <li><h3><a href = "Listado-ventas.php">Listado de Ventas</a></h3></li>
              <li><h3><a href = "Formas-pago.php">Formas de Pago</a></h3></li>
          </ul>
          <div class="container-login100-form-btn">
          <br/><br/><br/><br/><br/>
      </div>
          
          <ul>
          <p style="color: yellow">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Menu Secundario</p>
              <li><h3><a href = "Movimientos-caja.php">Flujo de caja</a></h3></li>
             
          </ul>
	  <?php
	  }
	  ?>
	  </br></br></br></br></br></br></br></br>


   </body>
   
</html>