<?php

include('session.php');
?>
<!DOCYPE html>

<html lang="en">

 

<head>

 <title>Registrar Usuario</title>

 <meta charset = "utf-8">

</head>

 

<body>

 <header>

 </header> 
 <style>
.tooltip {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 120px;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;

  /* Position the tooltip */
  position: absolute;
  z-index: 1;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}
</style>

 

 <form action="registrar-usuario.php" method="post"> 

 <hr />

 <h3>Crea un Usuario</h3>
 <!--Nombre Usuario-->

 <label for="nombre">Nombre de Usuario:</label><br>

 <input type="text" name="username" maxlength="8" required>
 <div class="tooltip">?
  <span class="tooltiptext">Usuario debe tener primera letra mayuscula</span>
</div>

 <br/><br/>
 <!--Password-->

 <label for="pass">Password:</label><br>

 <input type="password" name="password" maxlength="8" required>
 <div class="tooltip">?
  <span class="tooltiptext">password debe tener 8 caracteres</span>
</div>
 <br/><br/>
 <label for="perfil">Perfil:</label><br>

  <!--Id y nombre del perfil-->
 <select name="perfil" required>
     <?php
 
   $ses_sql = mysqli_query($db,"select Id_Perfil, Perfil from perfil");
   
   while($row = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC))
   {
	   echo "<option>".$row['Id_Perfil']." - ".$row['Perfil']. "</option>";
   }
   
?>

</select>

<div class="tooltip">?
  <span class="tooltiptext">Se debe seleccionar un perfil de usuario</span>
</div>

 <br/><br/>

 <input type="submit" name="submit" value="Crear">

 <input type="reset" name="clear" value="Borrar">

 </form>

 
 

 </body>

<br/><a href = "welcome.php"> Volver </a>
</html>
