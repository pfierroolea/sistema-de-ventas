DROP TABLE IF EXISTS bodega;

CREATE TABLE `bodega` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO bodega VALUES("1");
INSERT INTO bodega VALUES("2");



DROP TABLE IF EXISTS caja;

CREATE TABLE `caja` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO caja VALUES("1");
INSERT INTO caja VALUES("2");



DROP TABLE IF EXISTS perfil;

CREATE TABLE `perfil` (
  `Id_Perfil` int(11) NOT NULL AUTO_INCREMENT,
  `Perfil` varchar(20) NOT NULL,
  PRIMARY KEY (`Id_Perfil`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

INSERT INTO perfil VALUES("1","Administrador");
INSERT INTO perfil VALUES("2","Vendedor");
INSERT INTO perfil VALUES("3","Bodeguero");
INSERT INTO perfil VALUES("4","Caja");



DROP TABLE IF EXISTS usuario;

CREATE TABLE `usuario` (
  `Id_Usuario` int(12) NOT NULL AUTO_INCREMENT,
  `Login` varchar(8) NOT NULL,
  `Password` varchar(8) NOT NULL,
  `Id_Perfil` int(11) NOT NULL,
  PRIMARY KEY (`Id_Usuario`),
  KEY `fk_id_perfil` (`Id_Perfil`),
  CONSTRAINT `fk_id_perfil` FOREIGN KEY (`Id_Perfil`) REFERENCES `perfil` (`Id_Perfil`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

INSERT INTO usuario VALUES("1","pafierro","pafierro","1");
INSERT INTO usuario VALUES("2","bodega","bodega","3");
INSERT INTO usuario VALUES("3","cajero","cajero","4");
INSERT INTO usuario VALUES("4","vendedor","vendedor","2");
INSERT INTO usuario VALUES("12","Jose","P2020f","4");



DROP TABLE IF EXISTS venta;

CREATE TABLE `venta` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO venta VALUES("1");
INSERT INTO venta VALUES("2");



