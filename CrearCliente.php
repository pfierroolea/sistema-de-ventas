<?php

include('session.php');
?>
<!DOCYPE html>

<html lang="en">

 

<head>

 <title>Crear Cliente</title>

 <meta charset = "utf-8">

</head>

 

<body>

 <header>

 </header> 
 <style>
.tooltip {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 120px;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;

  /* Position the tooltip */
  position: absolute;
  z-index: 1;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}
</style>

 

 <form action="registrar-cliente.php" method="post"> 

 <hr />

 <h3>Crear un cliente</h3>
 <!--Nombre Usuario-->

 <label for="nombre">Nombre de Usuario:</label><br>

 <input type="text" name="Nombre" maxlength="100" required>
 <div class="tooltip">?
  <span class="tooltiptext">Ingresar el nombre del cliente a crear</span>
</div>

 <br/><br/>
 <!--Rut-->

 <label for="pass">Rut:</label><br>

 <input type="text" name="Rut" maxlength="10" placeholder="12345678-9" required>
 <div class="tooltip">?
  <span class="tooltiptext">Ingresar sin puntos y con guion</span>
</div>
 <br/><br/>
 <!--Direccion-->

 <label for="pass">Direccion:</label><br>

 <input type="text" name="Direccion" maxlength="100" placeholder="calle 1 casa 2" required>
 <div class="tooltip">?
  <span class="tooltiptext">Ingresar calle y numero</span>
</div>
 <br/><br/>
 <!--Telefono-->

 <label for="pass">Telefono:</label><br>

 <input type="text" name="Telefono" maxlength="20" placeholder="+56....." required>
 <div class="tooltip">?
  <span class="tooltiptext">+56925896315</span>
</div>
<br/><br/>
 <!--Correo-->

 <label for="pass">Correo:</label><br>

 <input type="text" name="Correo" maxlength="100" placeholder="p@rtu.cl" required>
 
<br/><br/>

 <input type="submit" name="submit" value="Crear">

 <input type="reset" name="clear" value="Borrar">

 </form>

 
 

 </body>

<br/><a href = "welcome.php"> Volver </a>
</html>
