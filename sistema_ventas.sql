-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-09-2020 a las 04:44:55
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sistema_ventas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bodega`
--

CREATE TABLE `bodega` (
  `Id_bodega` int(100) NOT NULL,
  `Stock` int(8) NOT NULL,
  `Id_producto` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE `caja` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `caja`
--

INSERT INTO `caja` (`id`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `Id_cliente` int(100) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Rut` varchar(10) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `Telefono` int(20) NOT NULL,
  `Correo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`Id_cliente`, `Nombre`, `Rut`, `Direccion`, `Telefono`, `Correo`) VALUES
(1, 'Rodrigo Torres', '12345678-9', '11', 3444, 'fggt@err'),
(2, 'Rodrigo Torres', '12345678-9', 'Calle 11 1215', 2147483647, 'fggt@err');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_usuario`
--

CREATE TABLE `log_usuario` (
  `Id_Log` int(11) NOT NULL,
  `Login` varchar(8) NOT NULL,
  `Password` varchar(8) NOT NULL,
  `Id_Perfil` int(11) NOT NULL,
  `Accion` varchar(255) NOT NULL,
  `Fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `log_usuario`
--

INSERT INTO `log_usuario` (`Id_Log`, `Login`, `Password`, `Id_Perfil`, `Accion`, `Fecha`) VALUES
(1, 'juan', '123', 3, 'insert', '2020-09-06 20:03:49'),
(2, 'juan', '123', 3, 'update', '2020-09-06 20:07:43'),
(3, 'juan', '123', 1, 'update', '2020-09-06 20:08:04'),
(4, 'juan', '123', 2, 'delete', '2020-09-06 20:09:35'),
(5, 'dddddddd', 'dddddddd', 4, 'insert', '2020-09-06 20:30:46'),
(8, 'dddddddd', 'ffffffff', 1, 'delete', '2020-09-07 02:06:45'),
(9, 'Carlos', '12345678', 4, 'insert', '2020-09-07 02:35:40'),
(10, 'Carlos', '12345678', 4, 'delete', '2020-09-07 03:26:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil`
--

CREATE TABLE `perfil` (
  `Id_Perfil` int(11) NOT NULL,
  `Perfil` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `perfil`
--

INSERT INTO `perfil` (`Id_Perfil`, `Perfil`) VALUES
(1, 'Administrador'),
(2, 'Vendedor'),
(3, 'Bodeguero'),
(4, 'Caja');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `Id_producto` int(100) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Precio` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`Id_producto`, `Nombre`, `Precio`) VALUES
(1, 'ropa', 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `Id_Usuario` int(12) NOT NULL,
  `Login` varchar(8) NOT NULL,
  `Password` varchar(8) NOT NULL,
  `Id_Perfil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`Id_Usuario`, `Login`, `Password`, `Id_Perfil`) VALUES
(1, 'pafierro', 'pafierro', 1),
(2, 'bodega', 'bodega', 3),
(3, 'cajero', 'cajero', 4),
(4, 'vendedor', 'vendedor', 2),
(5, 'Jose', 'P2020f', 2);

--
-- Disparadores `usuario`
--
DELIMITER $$
CREATE TRIGGER `after_delete_usuario` AFTER DELETE ON `usuario` FOR EACH ROW INSERT INTO log_usuario
 SET Accion = 'delete',
     Login = OLD.Login,
     Password = OLD.Password,
     Id_Perfil  = OLD.Id_Perfil ,
     Fecha = NOW()
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_insert_usuario` BEFORE INSERT ON `usuario` FOR EACH ROW INSERT INTO log_usuario(`Login`,`Password`,`Id_Perfil`,`Accion`,`Fecha`)
        VALUES(new.Login,new.Password,new.Id_Perfil,'insert',now())
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_update_usuario` BEFORE UPDATE ON `usuario` FOR EACH ROW INSERT INTO log_usuario
 SET Accion = 'update',
     Login = OLD.Login,
     Password = OLD.Password,
     Id_Perfil  = OLD.Id_Perfil ,
     Fecha = NOW()
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `Id_venta` int(11) NOT NULL,
  `Id_producto` int(100) NOT NULL,
  `precio` int(11) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Id_vendedor` int(12) NOT NULL,
  `Id_cliente` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bodega`
--
ALTER TABLE `bodega`
  ADD PRIMARY KEY (`Id_bodega`),
  ADD KEY `fk_id_producto` (`Id_producto`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`Id_cliente`);

--
-- Indices de la tabla `log_usuario`
--
ALTER TABLE `log_usuario`
  ADD PRIMARY KEY (`Id_Log`);

--
-- Indices de la tabla `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`Id_Perfil`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`Id_producto`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`Id_Usuario`),
  ADD KEY `fk_id_perfil` (`Id_Perfil`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`Id_venta`),
  ADD KEY `fk_id_vendedor` (`Id_vendedor`),
  ADD KEY `fk_id_cliente` (`Id_cliente`),
  ADD KEY `fk_id_productos` (`Id_producto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bodega`
--
ALTER TABLE `bodega`
  MODIFY `Id_bodega` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `Id_cliente` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `log_usuario`
--
ALTER TABLE `log_usuario`
  MODIFY `Id_Log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `perfil`
--
ALTER TABLE `perfil`
  MODIFY `Id_Perfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `Id_producto` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `Id_Usuario` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `Id_venta` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bodega`
--
ALTER TABLE `bodega`
  ADD CONSTRAINT `fk_id_producto` FOREIGN KEY (`Id_producto`) REFERENCES `productos` (`Id_producto`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_id_perfil` FOREIGN KEY (`Id_Perfil`) REFERENCES `perfil` (`Id_Perfil`);

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `fk_id_cliente` FOREIGN KEY (`Id_cliente`) REFERENCES `clientes` (`Id_cliente`),
  ADD CONSTRAINT `fk_id_productos` FOREIGN KEY (`Id_producto`) REFERENCES `productos` (`Id_producto`),
  ADD CONSTRAINT `fk_id_vendedor` FOREIGN KEY (`Id_vendedor`) REFERENCES `usuario` (`Id_Usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
